from pymongo import MongoClient
from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import MeanShift
from sklearn.cluster import AffinityPropagation
from sklearn.cluster import SpectralClustering
from sklearn.cluster import DBSCAN
import numpy



def get_user_names(db_name):
	client=MongoClient()
	names=client[db_name].user_names
	assert(names.find().count()==1),'problem in the list of user_names: either more than 1 list was saved or no lists were saved'
	names=names.find()[0]['user_ids']
	names=[str(item) for item in names]
	return names



def get_training_vectors(db_name,features_dataset):
	client=MongoClient()
	datasets=client[db_name].features_datasets
	assert(datasets.find({'features_dataset':features_dataset}).count()>0), 'the collection '+features_dataset+' does not exist'
	training_vectors=[]
	user_ids=get_user_names(db_name)
	for user_id in user_ids:
		found=datasets.find({'user_id':user_id, 'features_dataset':features_dataset})
		if found.count()==0:
			assert(0), 'the user_id '+user_id+' is missing'
		if found.count()>1:
			assert(0), 'the user_id '+user_id+' has more than one feature vector'
		assert(found.count()==1)
		values=found[0]['values']
		training_vectors.append(values)
	return training_vectors



def kmeans(db_name,features_dataset,n_cl):
	kmeans=KMeans(n_clusters=n_cl,init="k-means++",n_init=10, max_iter=300, tol=0.00001, precompute_distances=True, verbose=0, random_state=None, copy_x=True, n_jobs=1)
	user_ids=get_user_names(db_name)
	training_vectors=get_training_vectors(db_name,features_dataset)
	classes=kmeans.fit_predict(training_vectors)

	classes=map(int,list(classes))

	algorithm='k-means_'+str(n_cl)+'_clusters'
	client=MongoClient()
	out=client[db_name].clustering_results
	assert(out.find({"algorithm":algorithm, "features_dataset":features_dataset}).count()==0), 'a classification for '+algorithm+' on the features dataset '+features_dataset+" is already stored"
	out.insert({"algorithm":algorithm, "features_dataset":features_dataset, "classes": classes, "user_ids":user_ids})
	return


#possibilities for link: 'ward', 'complete', 'average'
def hierarchical(db_name,features_dataset,n_cl,link):
	ward=AgglomerativeClustering(n_clusters=n_cl, linkage=link)
	user_ids=get_user_names(db_name)
	training_vectors=get_training_vectors(db_name,features_dataset)
	classes=ward.fit_predict(training_vectors)

	classes=map(int,list(classes))
	algorithm='agglomerative_'+link+'_'+str(n_cl)+'_clusters'

	client=MongoClient()
	out=client[db_name].clustering_results
	assert(out.find({"algorithm":algorithm, "features_dataset":features_dataset}).count()==0), 'a classification for '+algorithm+' on the features dataset '+features_dataset+" is already stored"
	out.insert({"algorithm":algorithm, "features_dataset":features_dataset, "classes": classes, "user_ids":user_ids})
	return

def meanshift(db_name,features_dataset):
	meanshift=MeanShift()
	user_ids=get_user_names(db_name)
	training_vectors=get_training_vectors(db_name,features_dataset)
	classes=meanshift.fit_predict(numpy.array(training_vectors))

	classes=map(int,list(classes))
	algorithm='meanshift'
	client=MongoClient()
	out=client[db_name].clustering_results
	assert(out.find({"algorithm":algorithm, "features_dataset":features_dataset}).count()==0), 'a classification for '+algorithm+' on the features dataset '+features_dataset+" is already stored"
	out.insert({"algorithm":algorithm, "features_dataset":features_dataset, "classes": classes, "user_ids":user_ids})
	return

def affinity_propagation(db_name,features_dataset):
	affinitypropagation=AffinityPropagation()
	user_ids=get_user_names(db_name)
	training_vectors=get_training_vectors(db_name,features_dataset)
	classes=affinitypropagation.fit_predict(numpy.array(training_vectors))

	classes=map(int,list(classes))
	algorithm='affinity_propagation'
	client=MongoClient()
	out=client[db_name].clustering_results
	assert(out.find({"algorithm":algorithm, "features_dataset":features_dataset}).count()==0), 'a classification for '+algorithm+' on the features dataset '+features_dataset+" is already stored"
	out.insert({"algorithm":algorithm, "features_dataset":features_dataset, "classes": classes, "user_ids":user_ids})
	return

def spectral(db_name,features_dataset, n_cl):
	spectral=SpectralClustering(n_clusters=n_cl)
	user_ids=get_user_names(db_name)
	training_vectors=get_training_vectors(db_name,features_dataset)
	classes=affinitypropagaiton.fit_predict(numpy.array(training_vectors))

	classes=map(int,list(classes))
	algorithm='spectral_clustering_'+str(n_cl)+'_clusters'
	client=MongoClient()
	out=client[db_name].clustering_results
	assert(out.find({"algorithm":algorithm, "features_dataset":features_dataset}).count()==0), 'a classification for '+algorithm+' on the features dataset '+features_dataset+" is already stored"
	out.insert({"algorithm":algorithm, "features_dataset":features_dataset, "classes": classes, "user_ids":user_ids})
	return

def dbscan(db_name,features_dataset, epsilon):
	dbscan=DBSCAN(eps=epsilon)
	user_ids=get_user_names(db_name)
	training_vectors=get_training_vectors(db_name,features_dataset)
	classes=affinitypropagaiton.fit_predict(numpy.array(training_vectors))

	classes=map(int,list(classes))
	algorithm='dbscan'
	client=MongoClient()
	out=client[db_name].clustering_results
	assert(out.find({"algorithm":algorithm, "features_dataset":features_dataset}).count()==0), 'a classification for '+algorithm+' on the features dataset '+features_dataset+" is already stored"
	out.insert({"algorithm":algorithm, "features_dataset":features_dataset, "classes": classes, "user_ids":user_ids})
	return

