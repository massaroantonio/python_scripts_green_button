from pymongo import MongoClient
from lxml import etree
present_tz_offset=3600

#creates an element of the form {"time_seconds":int,"time_date":string,"value":int, "duration":int} and stores it in the collection "user" inside the database db_name. the name of the collection is stored in the first entry of the list "fields", the other entries carry the values of the element to be stored
def insert_one_consumer_data_in_db(fields, db_name):
	client=MongoClient()
	db=client[db_name]
	i=0
	user=fields[0]
	element={"time_stamp":fields[1],"date_time":fields[2],"weekday":fields[3], "time_offset":fields[4],"duration":fields[5], "value":fields[6]}
	db[user].insert(element)
	i=i+1
	#print("inserted line "+str(i))
	return

#stores the data contained in a green button xml file in a collection named after the xml file itself, inside the database db_name
def load_gb_file_in_db(gb_file_name,db_name,xmlschema):
	import datetime
	fields=["N/A" for i in range(7)]
	user_id="user_"+gb_file_name.split("/")[-1][:-4]
	fields[0]=user_id
	xmlschema_doc=etree.parse(xmlschema)
	xmlschema=etree.XMLSchema(xmlschema_doc)

	tree=etree.parse(gb_file_name)
	root=tree.getroot()

	interval_block_list=[]
	namespace="http://naesb.org/espi"
	time_offset=root.find(".//{http://naesb.org/espi}tzOffset").text	
	if (isinstance(time_offset,basestring) and time_offset!=""):
		time_offset=int(time_offset)
	else:
		time_offset="N/A"
	

	interval_block_list= root.findall(".//{http://naesb.org/espi}IntervalBlock")
	starts=[]
	ends=[]
	for block in interval_block_list:
		interval=block.find(etree.QName(namespace,"interval"))
		start=int(interval.find(etree.QName(namespace,"start")).text)
		duration=int(interval.find(etree.QName(namespace,"duration")).text)
		starts.append(start)
		ends.append(start+duration)
	
	for i in range(len(starts)):
		for j in range(i):
			assert(end[i]<start[j] or end[j]<start[i]), 'overlapping time periods for different IntervalBlocks'



	for block in interval_block_list:
		assert xmlschema(block), 'an interval-block does not comply with the xml schema for greenbutton xml files'
		print('ok interval block compatible')
		intervals=block.findall(".//{http://naesb.org/espi}IntervalReading")
		for interval in intervals:
			for x in interval.iter():
				if isinstance(x.tag, basestring):
					tag=etree.QName(x)
					if tag.localname=="start":
						if (isinstance(x.text, basestring) and x.text!=''):
							fields[1]=int(x.text)
							fields[2]=datetime.datetime.fromtimestamp(fields[1]+time_offset-present_tz_offset) 
							fields[3]=fields[2].weekday()  
						else :
							fields[1]='N/A'
							fields[2]='N/A'
						 	fields[3]='N/A'
					fields[4]=time_offset
					if tag.localname=="duration":
						if (isinstance(x.text, basestring) and x.text!=''):
							fields[5]=int(x.text)  
						else:
						 	fields[5]='N/A'
					if tag.localname=="value":
						if (isinstance(x.text, basestring) and x.text!=''):
							fields[6]=int(x.text)
						else:
							fields[6]='N/A' 
			insert_one_consumer_data_in_db(fields, db_name)
			fields=["N/A" for i in fields]
			fields[0]=user_id

	interval_length=root.find(".//{http://naesb.org/espi}intervalLength").text
	if (isinstance(interval_length,basestring) and interval_length!=""):
		interval_length=int(interval_length)  
	else:
		interval_length="N/A"
	client=MongoClient()
	client[db_name][user_id].insert({"interval_length":interval_length})
	return

#stores all the green button xml files inside a directory in the database db, each file will be stored in a different collection, named after the file itself. 
#reminder: try to store all the names inside load_gb_file_in_db() by appending to a list stored in a collection, it is more elegant
def load_all_gb_files_in_db(directory, db_name, xmlschema):   
	from os import listdir
	from os.path import isfile, join
	user_ids=[]
	onlyfiles = [ join(directory,f) for f in listdir(directory) if isfile(join(directory,f)) ]
	for f in onlyfiles:
		if(f[-3:]=='xml'):
			user_id="user_"+f.split("/")[-1][:-4]
			user_ids.append(user_id)
			print("inserting data for "+f)
			load_gb_file_in_db(f,db_name,xmlschema)
			print("inserted data for "+f+"\n")
	client=MongoClient()
	client[db_name].user_names.insert({"user_ids":user_ids})
	return