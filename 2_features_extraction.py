from pymongo import MongoClient
import matplotlib.pyplot as plt
import datetime
from os import listdir
import numpy as np



def get_user_names(db_name):
	client=MongoClient()
	names=client[db_name].user_names
	assert(names.find().count()==1),'problem in the list of user_names: either more than 1 list was saved or no lists were saved'
	names=names.find()[0]['user_ids']
	return [str(item) for item in names]

def get_outliers_data(db,outliers_class,user_id):
	client=MongoClient()
	outl=client[db].corrected_outliers.find({'user_id':user_id,"outliers_class":outliers_class}).sort('date_time')
	timestamps=[]
	corrected_outl=[]
	for item in outl:
		timestamps.append(item['time_stamp'])
		corrected_outl.append(item['value'])

	return [timestamps,corrected_outl]

#returns a list of two lists, the first contains the datetimes of observations,the second contains the respective measurements. The lists are ordered by date
def daily_time_window(db_name,outliers_data,user_id,datetime_start):
	client=MongoClient()
	f=client[db_name][user_id].find({'interval_length':{'$exists':True}})
	assert(f.count()==1)
	interval_length=int(f[0]['interval_length'])
	size=int(60*60*24/interval_length)
	timestamps=[-1]*size
	values=[0]*size

	datetime_end=datetime_start.replace(hour=23, minute=59, second=59)
	
	outl_timestamps=outliers_data[0]
	corrected_outl=outliers_data[1]
	for z in client[db_name][user_id].find({"date_time":{"$gte":datetime_start ,"$lte":datetime_end}}).sort("date_time"):
		h=z['date_time'].hour
		m=z['date_time'].minute
		assert(m%5==0)
		index=int(m/5+h*12)
		if z['time_stamp'] in outl_timestamps:
			outl_index=outl_timestamps.index(z['time_stamp'])
			timestamps[index]=(outl_timestamps[outl_index])
			values[index]=(corrected_outl[outl_index])
		else:
			timestamps[index]=(z["time_stamp"])
			values[index]=(z["value"])

	return [timestamps,values]
	
#returns two lists in a dictionary, the first contains the timestamps of observations, arranged by [day][time] the second contains the respective measurements, arranged in the same way [day][time]
def get_days_in_range(db_name,outliers_class, user_id, datetime_start, datetime_end):
	client=MongoClient()
	#inserire controlli sulle date
	timestamps=[]
	values=[]
	

	#i consider the interval [datetime_start, datetime_end] intersected [min_date for user_id, max_date for user_id]
	v=client[db_name][user_id].find({"date_time":{"$gte":datetime_start ,"$lte":datetime_end}}).sort("date_time")
	datetime_start=max(v[0]["date_time"],datetime_start)
	for vv in v:	
		pass
	datetime_end=min(vv["date_time"],datetime_end)
	
	if outliers_class=='all_outliers':
		outliers_data=[[],[]]
	else:
		outliers_data=get_outliers_data(db_name,outliers_class,user_id)
	z=daily_time_window(db_name,outliers_data, user_id, datetime_start)
	
	timestamps.append(z[0])
	values.append(z[1])
	start=(datetime_start+datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0,microsecond=0)
	while((datetime_end-start).days>0):
		#one day measurement
		z=daily_time_window(db_name,outliers_data, user_id, start)
		timestamps.append(z[0])
		values.append(z[1])
		start=(start+datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0,microsecond=0)
	#one day measurement
	z=daily_time_window(db_name,outliers_data, user_id, start)
	
	timestamps.append(z[0])
	values.append(z[1])
	output={"timestamps": timestamps, "values": values}
	return output

#returns a list of measurements that represent the average daily consumption between datetime_start and datetime_end
def get_avg_day(db_name,outliers_class, user_id, datetime_start, datetime_end):
	client=MongoClient()
	
	f=client[db_name][user_id].find({'interval_length':{'$exists':True}})
	assert(f.count()==1)
	interval_length=f[0]['interval_length']
	n_values_in_day=int(60*60*24/interval_length)
	average_day=[0]*n_values_in_day
	count=[0]*n_values_in_day

	m=get_days_in_range(db_name,outliers_class, user_id, datetime_start, datetime_end)
	n_days=len(m["values"])
	assert(n_days>=3)
	for d in m['values']:
		for i in range(n_values_in_day):
			average_day[i]=average_day[i]+d[i]
	
	for d in m['timestamps']:
		for i in range (n_values_in_day):
			if d[i]==-1:
				count[i]=count[i]+1
	
	for i in range(n_values_in_day):
			average_day[i]=average_day[i]/float(n_days-count[i]) 
	return average_day

#stores to db.collection_name the average daily consumption of all the users
def store_avg_day_users(db_name,outliers_class, datetime_start,datetime_end):
	features_dataset_name=str(outliers_class)+'_avg_day_from_'+str(datetime_start)+"_to_"+str(datetime_end)

	client=MongoClient()
	user_ids=get_user_names(db_name)
	out=client[db_name].features_datasets
	assert(out.find({"features_dataset":features_dataset_name}).count()==0), 'the selected features set already exists'

	for user_id in user_ids:
		print(str(user_id)+"_begin...")
		w=get_avg_day(db_name,outliers_class,str(user_id),datetime_start,datetime_end)
		out.insert({"user_id":user_id, "values": w, "features_dataset":features_dataset_name})
		print(str(user_id)+"_done!")
		w=[]
	return

#stores to db.collection_name the normalized average daily consumption of all the users
def store_normalized_avg_day_users(db_name,outliers_class,datetime_start,datetime_end):
	features_dataset_name=str(outliers_class)+"_normalized_avg_day_from_"+str(datetime_start)+"_to_"+str(datetime_end)

	client=MongoClient()
	user_ids=get_user_names(db_name)	
	out = client[db_name].features_datasets
	assert(out.find({"features_dataset":features_dataset_name}).count()==0), 'the selected features set already exists'
	for user_id in user_ids:
		print(str(user_id)+"_begin...")
		w=get_avg_day(db_name,outliers_class,str(user_id),datetime_start,datetime_end)
		m=min(w)
		M=max(w)
		w=[(float(item)-m)/(M-m) for item in w]
		out.insert({"user_id":user_id, "values": w, "features_dataset":features_dataset_name})
		print(str(user_id)+"_done!")
		w=[]
	return

