from pymongo import MongoClient
import matplotlib.pyplot as plt
import datetime
import pylab as P
from os import listdir
import numpy as np

def get_missing(db,user_id):
	client=MongoClient()
	db=client[db]
	records=db[user_id].find({'value':{"$exists":True}}).sort('time_stamp')
	print (records.count())
	interval_record=db[user_id].find({'interval_length':{'$exists':True}})
	t_offset=records[0]['time_offset']
	assert(interval_record.count()==1)
	interval=interval_record[0]['interval_length']
	miss_t_start=[]
	miss_t_end=[]
	miss_v_start=[]
	miss_v_end=[]
	timestamps=[]
	values=[]
	for record in records:
		timestamps.append(record['time_stamp'])
		values.append(record['value'])
	for i in range(1,len(timestamps)):
		assert(timestamps[i]>timestamps[i-1])

	for i in range(1,len(timestamps)):
		if(int(timestamps[i])-int(timestamps[i-1])!=interval):
			check=0
			miss_t_start.append(timestamps[i-1])
			miss_t_end.append(timestamps[i])
			miss_v_start.append(values[i-1])
			miss_v_end.append(values[i])
	return[miss_t_start,miss_t_end,miss_v_start,miss_v_end, interval, t_offset]


def get_0_negative_report(db, report_file):
	user_ids=client[db].user_names.find()[0]['user_ids']
	for user in user_ids:
		user_coll=client[db][user]
		z=user_coll.find({'value':0}).count()
		y=user_coll.find({'value':{'$lt':0}}).count()
		if(y!=0 or z!=0):
			print(user+': negatives '+str(y)+', zeros '+str(z))
			report=open(report_file,'a')
			report.write(user+': negatives '+str(y)+', zeros '+str(z)+'\n')
			report.close()
	return

def interpol(t_start,t_end,v_start,v_end,interval,t_offset,target_db,user_id):
	
	client=MongoClient()
	delta_t=t_end-t_start
	delta_v=(v_end-v_start)/(float(delta_t)/interval)
	n_missing=delta_t/interval-1
	for j in range(1,n_missing+1):
		inter_t=t_start+j*interval
		inter_t_shift=inter_t-my_offset+t_offset
		interpol_v=v_start+j*delta_v
		date_time=datetime.fromtimestamp(inter_t_shift)
		weekday=date_time.weekday()	
		element={"time_stamp":inter_t,"date_time":date_time,"weekday":weekday, "time_offset":t_offset,"duration":interval, "value":interpol_v}
		client[target_db][user_id].insert(element)			
	return

def remove_entire_day(t_start,t_end,target_db,user_id):
	client=MongoClient()
	date_start=datetime.fromtimestamp(t_start)
	date_end=datetime.fromtimestamp(t_end)
	date_start_floor=date_start.replace(hour=0, minute=0,second=0,microsecond=0)
	date_end_roof=date_start.replace(hour=23, minute=59,second=59,microsecond=0)
	client[target_db][user_id].remove({'date_time':{'$gt':date_start_floor,'$lt':date_end_roof}})
	return

def clean(db,max_missing=12):
	user_ids=client[db].user_names.find()[0]['user_ids']

	for user_id in user_ids:
		v=get_missing(db,user_id)
		if (len(v[0])==0):
			print(user_id+' has no missing data')
		else:
			print(user_id+' HAS MISSING DATA')
			
			update_report_in_db(db,user_id,v)
			t_start=v[0]
			t_end=v[1]
			v_start=v[2]
			v_end=v[3]
			interval=v[4]
			t_offset=v[5]
			
			for i in range(len(v[0])):
				if((t_end[i]-t_start[i])/interval<=max_missing):
					interpol(t_start[i],t_end[i],v_start[i],v_end[i],interval,t_offset,db,user_id)
				else:
					remove_entire_day(t_start[i],t_end[i],db,user_id)
				
	return

def update_report_in_db(db,user_id,v):
	t=[]
	t_start=v[0]
	t_end=v[1]
	v_start=v[2]
	v_end=v[3]
	interval=v[4]
	t_offset=v[5]
	datetimes=[]
	for timestamp in t_start:
		datetimes.append(datetime.fromtimestamp(timestamp+t_offset-my_offset))
	for i in range(len(t_start)):
		t.append((t_end[i]-t_start[i])/interval-1)
	tot_n_missing=sum(t)
	out=client[db].missing_report
	element={'user_id':user_id,'tot_n_missing':tot_n_missing,'detail_n_missing':t,'detail_date_missing':datetimes}
	out.insert(element)
	return

def get_report(db):
	client=MongoClient()
	f=client[db].missing_report.find()
	for z in f:
		detail_n_missing=''
		detail_date_missing=''
		user=z['user_id']
		tot_n_missing=str(z['tot_n_missing'])
		detail_n_missing=z['detail_n_missing']
		detail_date_missing=z['detail_date_missing']
		print(user+': '+tot_n_missing)
		for i in range (len(z['detail_n_missing'])):
			print(str(detail_date_missing[i])+": "+str(detail_n_missing[i]))
		print('------')
	return

def get_outliers_by_coherent_timeframes(db, thresh):
	client=MongoClient()
	user_ids=client[db].user_names.find()[0]['user_ids']
	
	
	for user_id in user_ids:
		records=client[db][user_id].find({'value':{'$exists':True}}).sort('time_stamp')

		records_summer=[[[] for i in range(24)] for j in range (7)]
		records_winter=[[[] for i in range(24)] for j in range (7)]

		values_summer=[[[] for i in range(24)] for j in range (7)]
		values_winter=[[[] for i in range(24)] for j in range (7)]
		
		up_q_winter=[[0 for i in range(24)] for j in range (7)]
		low_q_winter=[[0 for i in range(24)] for j in range (7)]
		up_q_summer=[[0 for i in range(24)] for j in range (7)]
		low_q_summer=[[0 for i in range(24)] for j in range (7)]

		mean_winter=[[0 for i in range(24)] for j in range (7)]
		mean_summer=[[0 for i in range(24)] for j in range (7)]

		up_outl_winter=[[[] for i in range(24)] for j in range (7)]
		low_outl_winter=[[[] for i in range(24)] for j in range (7)]
		up_outl_summer=[[[] for i in range(24)] for j in range (7)]
		low_outl_summer=[[[] for i in range(24)] for j in range (7)]

		found_outl_winter=[[[] for i in range(24)] for j in range (7)]
		found_outl_summer=[[[] for i in range(24)] for j in range (7)]		

		for record in records:
			weekday=record['weekday']
			hour=record['date_time'].hour
			if record['date_time'].month in [9,10,11,12,1,2,3,4]:
				records_winter[weekday][hour].append(record)
				values_winter[weekday][hour].append(record['value'])
			else:	
				records_summer[weekday][hour].append(record)
				values_summer[weekday][hour].append(record['value'])

		
		

		for i in range(7):
			for j in range(24):
				up_q_winter[i][j]=np.percentile(values_winter[i][j],95,interpolation='nearest')
				low_q_winter[i][j]=np.percentile(values_winter[i][j],5,interpolation='nearest')
				up_q_summer[i][j]=np.percentile(values_summer[i][j],95,interpolation='nearest')
				low_q_summer[i][j]=np.percentile(values_summer[i][j],5,interpolation='nearest')
				mean_winter[i][j]=np.mean(values_winter[i][j])
				mean_summer[i][j]=np.mean(values_summer[i][j])
		
		for i in range(7):
			for j in range(24):
				up_outl_winter[i][j]=[z for z in records_winter[i][j] if z['value']>up_q_winter[i][j]]
				low_outl_winter[i][j]=[z for z in records_winter[i][j] if z['value']<low_q_winter[i][j]]
				up_outl_summer[i][j]=[z for z in records_summer[i][j] if z['value']>up_q_summer[i][j]]
				low_outl_summer[i][j]=[z for z in records_summer[i][j] if z['value']<low_q_summer[i][j]]


		for i in range(7):
			for j in range(24):
				t_up_winter=thresh*(up_q_winter[i][j]-mean_winter[i][j])
				t_down_winter=thresh*(mean_winter[i][j]-low_q_winter[i][j])
				m=mean_winter[i][j]
				u=[z for z in up_outl_winter[i][j] if z['value']-m>t_up_winter]
				l=[z for z in low_outl_winter[i][j] if m-z['value']>t_down_winter]
				found_outl_winter[i][j]=u+l
				u=[]
				l=[]
				t_up_summer=thresh*(up_q_summer[i][j]-mean_summer[i][j])
				t_down_summer=thresh*(mean_summer[i][j]-low_q_summer[i][j])
				m=mean_summer[i][j]
				u=[z for z in up_outl_summer[i][j] if z['value']-m>t_up_summer]
				l=[z for z in low_outl_summer[i][j] if m-z['value']>t_down_summer]
				found_outl_summer[i][j]=u+l
				u=[]
				l=[]
		
		
		report_outliers_in_db(found_outl_winter,found_outl_summer,thresh,user_id,db)
		
		found_outl_winter_corrected=correct_outliers(values_winter,records_winter,found_outl_winter,15)
		found_outl_summer_corrected=correct_outliers(values_summer,records_summer,found_outl_summer,15)
		
		for i in range(7):
			for j in range(24):
				for record in found_outl_summer_corrected[i][j]:
					element={"user_id":user_id,"outliers_class":'quantile_thresh_'+str(thresh) ,"time_stamp":record['time_stamp'],"date_time":record['date_time'],"weekday":record['weekday'], "time_offset":record['time_offset'],"duration":record['duration'], "value":record['value']}
					client[db].corrected_outliers.insert(element)
				for record in found_outl_winter_corrected[i][j]:
					element={"user_id":user_id,"outliers_class":'quantile_thresh_'+str(thresh) ,"time_stamp":record['time_stamp'],"date_time":record['date_time'],"weekday":record['weekday'], "time_offset":record['time_offset'],"duration":record['duration'], "value":record['value']}
					client[db].corrected_outliers.insert(element)

		print('threshold:',thresh,'user: ',user_id)
	return

def correct_outliers(data_values,data_records,found_outliers,average_window):
	k=-1
	t=-1
	for i in range(7):
		for j in range(24):
			for record in found_outliers[i][j]:	
				t=data_values[i][j].index(record['value'])
				left=min(0,t-average_window)
				right=max(t+average_window,len(data_values[i][j]))
				record['value']=sum(data_values[i][j][left:right])/float(right-left)
	return found_outliers

def report_outliers_in_db(found_outl_winter,found_outl_summer,thresh,user_id,db):
	client=MongoClient()
	count=[[0 for i in range(24)] for j in range(7)]
	date_times=[]	
	for i in range(7):
		for j in range(24):
			count[i][j]=count[i][j]+len(found_outl_winter[i][j])+len(found_outl_summer[i][j])
	if sum(list(map(sum,count))):			
		for i in range (7):
			for j in range(24):			
					for record in found_outl_summer[i][j]+found_outl_winter[i][j]:
						date_times.append(record['date_time'])
		record={'outliers_class':'quantile_thresh_'+str(thresh), 'user_id':user_id,'n_outliers':sum(list(map(sum,count))),'detail_outliers':count,'date_time':date_times}
		client[db].outliers_report.insert(record)
	#	print(record)
	return 
