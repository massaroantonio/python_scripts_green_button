from pymongo import MongoClient
import datetime
import matplotlib.pyplot as plt
import numpy
import math

#returns a list of lists of user_id s, grouped by cluster out=[[usr_x,usr_y],[usr_z],[usr_w,usr_t,usr_a],...]
def get_clusters_user_id(db,features_dataset,algorithm):
	client=MongoClient()
	collection=client[db].clustering_results
	found=collection.find({"algorithm":algorithm, "features_dataset":features_dataset})
	assert(found.count()==1)
	user_ids=found[0]["user_ids"]
	classes=found[0]["classes"]
	n_classes=len(set(classes))
	out=[[] for i in set(classes)]
	for i in range(len(user_ids)):
		out[classes[i]].append(str(user_ids[i]))
		#print(out[0])
	return out
#returns a list of lists of feature vectors, grouped by cluster: cluster=[[feat_usr_x,feat_usr_y],[feat_usr_z],[feat_usr_w,feat_usr_t,feat_usr_a],...]
def get_clusters(db,features_dataset,algorithm):
	client=MongoClient()
	dataset=client[db].features_datasets

	cluster_user_id=get_clusters_user_id(db,features_dataset,algorithm)
	clusters=[[] for i in cluster_user_id]
	n_clusters=len(cluster_user_id)
	for i in  range(n_clusters):
		for user_id in cluster_user_id[i]:
			f=dataset.find({'features_dataset':features_dataset,'user_id':user_id})
			assert(f.count()==1), 'the vector for '+user_id+" does  not exist in the dataset "+features_dataset
			v=numpy.array(map(float,(f[0]['values'])))
			clusters[i].append(v)	

	return clusters


def get_centroids(db,features_dataset,algorithm):
	clusters=get_clusters(db,features_dataset,algorithm)
	n_cl=len(clusters)
	centroid=numpy.zeros(len(clusters[0][0]))
	centroids=[]
	for cluster in clusters:
		for item in cluster:
				centroid=centroid+item
		centroid=centroid/len(cluster)
		centroids.append(centroid)
		centroid=centroid*0
	return centroids

def get_centroids_from_clusters(clusters):
	n_cl=len(clusters)
	centroid=numpy.zeros(len(clusters[0][0]))
	centroids=[]
	for cluster in clusters:
		for item in cluster:
				centroid=centroid+item
		centroid=centroid/len(cluster)
		centroids.append(centroid)
		centroid=centroid*0
	return centroids

def avg_dist_within_set(set):
	n=len(set)
	if n==1:
		return 0
	d=0
	for i in range(n):
		for j in range(i):
			d=d+numpy.linalg.norm(set[i]-set[j])
	d=d/(n*(n-1)*0.5)
	return d	


def CDI_from_clusters(clusters):
	centroids=get_centroids_from_clusters(clusters)
	d=0
	for cluster in clusters:
		d=d+pow(avg_dist_within_set(cluster),2)
	CDI=math.sqrt(d/len(clusters))/avg_dist_within_set(centroids)
	return CDI

def MDI_from_clusters(clusters):
	centroids=get_centroids_from_clusters(clusters)
	D=[]
	for cluster in clusters:
		D.append(avg_dist_within_set(cluster))
	D_C=[]
	for i in range(len(centroids)):
		for j in range(i):
			D_C.append(numpy.linalg.norm(centroids[i]-centroids[j]))
	return max(D)/min(D_C)

def MSE_from_clusters(clusters):	
	centroids=get_centroids_from_clusters(clusters)
	MSE=0.0
	n_cl=len(centroids)
	n=0
	for cluster in clusters:
		n=n+len(cluster)
	for i in range(n_cl):
		cluster=clusters[i]
		centroid=centroids[i]
		for item in cluster:
			MSE=MSE+pow(numpy.linalg.norm(centroid-item),2)
	return MSE/n

def within_to_between_from_clusters(clusters):
	centroids=get_centroids_from_clusters(clusters)
	n_cl=len(centroids)
	n=.0
	d=.0
	for i in range(n_cl):
		cluster=clusters[i]
		centroid=centroids[i]
		for item in cluster:
			n=n+pow(numpy.linalg.norm(centroid-item),2)
	for i in range(n_cl):
		for j in range(i):
			d=d+pow(numpy.linalg.norm(centroids[i]-centroids[j]),2)
	return n/d

def DBI_from_clusters(clusters):
	centroids=get_centroids_from_clusters(clusters)
	n_cl=len(centroids)
	within=[]
	M=[]
	d=[]
	s=[]
	for cluster in clusters:
		d.append(avg_dist_within_set(cluster))
	for i in range(n_cl):
		for j in range(n_cl):
			if j!=i:
				s.append((d[i]+d[j])/numpy.linalg.norm(centroids[i]-centroids[j]))
		M.append(max(d))
		s=[]
	return sum(M)/n_cl


def measure_goodness(db,features_dataset,algorithm):
	client=MongoClient()
	out=client[db].goodness_measures
	clusters=get_clusters(db,features_dataset,algorithm)
	CDI=CDI_from_clusters(clusters)
	MDI=MDI_from_clusters(clusters)
	MSE=MSE_from_clusters(clusters)
	WTB=within_to_between_from_clusters(clusters)
	DBI=DBI_from_clusters(clusters)
	assert(out.find({"features_dataset":features_dataset,"algorithm":algorithm}).count()==0), "goodness measures have already been calculated for the features' dataset "+ features_dataset+" and algorithm "+algorithm
	out.insert({"features_dataset":features_dataset,
				"algorithm":algorithm, 
				"CDI":CDI,
				"MDI":MDI,
				"MSE":MSE,
				"WTB":WTB,
				"DBI":DBI})
	return
